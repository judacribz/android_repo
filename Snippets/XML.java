 try {
    // parse out the data
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
    Log.d("InternetResourcesSample", "url: " + params[0]);
    URL url = new URL(params[0]);
    Document document = docBuilder.parse(url.openStream());
    document.getDocumentElement().normalize();

    // look for <WordDefinition> tags
    NodeList main = document.getElementsByTagName("Definitions");
    if ((main.getLength() > 0) && (main.item(0).getNodeType() == Node.ELEMENT_NODE)) {
        Element definitions = (Element)main.item(0);
        NodeList defTags = definitions.getElementsByTagName("WordDefinition");
        definition = "";
        for (int i = 0; i < defTags.getLength(); i++) {
            Node def = defTags.item(i);
            definition += def.getTextContent() + "\n";
        }
    }

} catch (Exception e) {
    e.printStackTrace();
    exception = e;
}



//OR

ArrayList<Story> stories = new ArrayList<>();
try {
    // parse out the data
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
    URL url = new URL(urls[0]);
    Document document = docBuilder.parse(url.openStream());
    document.getDocumentElement().normalize();

    // look for <WordDefinition> tags
    NodeList main = document.getElementsByTagName("entry");
    if ((main.getLength() > 0) && (main.item(0).getNodeType() == Node.ELEMENT_NODE)) {


        for (int i=0; i <main.getLength(); i++) {

            Element definitions = (Element)main.item(i);

            stories.add(new Story(definitions.getElementsByTagName("title").item(0).getTextContent(),
                    definitions.getElementsByTagName("author").item(0).getTextContent(),
                    definitions.getElementsByTagName("summary").item(0).getTextContent()));
        }
    }

} catch (Exception e) {
    e.printStackTrace();
}