//EnterURLActivity
public class EnterURLActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_url);

        ((Button)findViewById(R.id.btnShowFeed)).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        String url = "http://www.theregister.co.uk/software/os/headlines.atom";
        Intent showFeedIntent = new Intent(this, ShowFeedActivity.class);
        showFeedIntent.putExtra("URL", url);
        startActivity(showFeedIntent);
    }

}


//ShowFeedActivity
public class ShowFeedActivity extends AppCompatActivity implements StoryDataListener{

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_feed);

        listView = findViewById(R.id.lstFeed);



        DownloadFeedTask task = new DownloadFeedTask();
        task.setStoryDataListener(this);
        String url = getIntent().getStringExtra("URL");
        task.execute(url);


    }

    @Override
    public void showStories(ArrayList<Story> data) {
        StoryAdapter storyAdapter = new StoryAdapter(this, data);
        listView.setAdapter(storyAdapter);
    }
}

//DownloadFeedTask
public class DownloadFeedTask extends AsyncTask<String, Void, ArrayList<Story>> {

    
    //Interface StoryDataListener
    private StoryDataListener storyDataListener;

    void setStoryDataListener(StoryDataListener storyDataListener) {

        this.storyDataListener = storyDataListener;
    }

    @Override
    protected ArrayList<Story> doInBackground(String... urls) {
        ArrayList<Story> stories = new ArrayList<>();
        try {
            // parse out the data
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
            URL url = new URL(urls[0]);
            Document document = docBuilder.parse(url.openStream());
            document.getDocumentElement().normalize();

            // look for <WordDefinition> tags
            NodeList main = document.getElementsByTagName("entry");
            if ((main.getLength() > 0) && (main.item(0).getNodeType() == Node.ELEMENT_NODE)) {


                for (int i=0; i <main.getLength(); i++) {

                    Element definitions = (Element)main.item(i);

                    stories.add(new Story(definitions.getElementsByTagName("title").item(0).getTextContent(),
                            definitions.getElementsByTagName("author").item(0).getTextContent(),
                            definitions.getElementsByTagName("summary").item(0).getTextContent()));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return stories;
    }

    @Override
    protected void onPostExecute(ArrayList<Story> stories) {

        // send back new data
        storyDataListener.showStories(stories);
    }
}


//StoryAdapter
public class StoryAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Story> data;

    public StoryAdapter(Context context, ArrayList<Story> data) {
        this.data = data;
        this.context = context;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Story storyToDisplay = data.get(position);

        Log.d("StoryAdapter", "Story:");
        Log.d("StoryAdapter", "  Title:   "+ storyToDisplay.getTitle());
        Log.d("StoryAdapter", "  Author:  "+storyToDisplay.getAuthor());
        Log.d("StoryAdapter", "  Content: "+storyToDisplay.getContent());

        if (convertView == null) {
            // create the layout
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_view_story_item, parent, false);
        }

        // populate the views with the data from story
        TextView lblTitle = (TextView)convertView.findViewById(R.id.lblTitle);
        lblTitle.setText(storyToDisplay.getTitle());

        TextView lblAuthor = (TextView)convertView.findViewById(R.id.lblAuthor);
        lblAuthor.setText(storyToDisplay.getAuthor());

        TextView lblContent = (TextView)convertView.findViewById(R.id.lblContent);
        lblContent.setText(storyToDisplay.getContent());

        return convertView;
    }
}


//Interface StoryDataListener
public interface StoryDataListener {
    public void showStories(ArrayList<Story> data);
}