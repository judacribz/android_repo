insert(String tableName, String nullColumnHack, ContentValues values)

query(String tableName, String[] columns, String selection, String[] selectionArgs, 
    String groupBy, String having, String orderBy) //this line optional
// groupBy - to give groups of data
// having - the where clause for groupBy

rawQuery(String sq1, String[] selectionArgs)

update(String tableName, ContentValues values, String whereClause, String[] whereArgs)

delete(String tableName, String whereClause, String[] whereArgs)

execSQL(String sql, Object[]bindArgs)