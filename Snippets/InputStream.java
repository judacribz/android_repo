StringBuilder content = new StringBuilder();

try {
    Uri uri = responseIntent.getData();
    InputStream inRaw = getContentResolver().openInputStream(uri);
    BufferedReader in = new BufferedReader(new InputStreamReader(inRaw)));
    String line = null;
    While((line = in.readLine()) != null) {
        content.append(line + "\n");
    }
    inRaw.close();
}
} catch (IOException io) {
    io.printStackTrace();
}

showFileContents(content.toString());


// OR
try {
    InputStream inputStream = activity.openFileInput(file);

    if (inputStream != null) {
        //or CSVReader reader = new CSVReader(new INputStreamReader)
        // reader.readNext() to get data
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(inputStream)
        );

        String line;
        Scanner scan;

        while((line = reader.readLine()) != null) {
            scan = new Scanner(line);
            contacts.add(new Contact(scan.nextInt(),
                                     scan.next(),
                                     scan.next(),
                                     scan.next()));
        }

        reader.close();
    }
} catch(FileNotFoundException fnf) {
    Log.d(activity.getLocalClassName(), file + " not found");
} catch (IOException io) {
    io.printStackTrace();
}


//Write

try {
    BufferedWriter writer = new BufferedWriter(
            new OutputStreamWriter(activity.openFileOutput(
                    file,
                    Context.MODE_PRIVATE
            ))
    );

    for (int i = 0; i < contacts.size(); i++) {
        writer.write(i + " " + contacts.get(i).getContactLine());
        writer.newLine();
    }

    writer.close();
}
catch (IOException io) {
    io.printStackTrace();
}
