package ca.uoit.mobileapp.fitnessapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class WorkoutsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts);
    }

    // Starts AddWorkoutActivity
    public void startAddWorkout(View source) {
        Intent addWorkout = new Intent(this, AddWorkoutActivity.class);
        startActivity(addWorkout);
    }
}


<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="ca.uoit.mobileapp.fitnessapp.WorkoutsActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:layout_marginTop="15dp"
        android:layout_marginStart="15dp"
        android:layout_marginEnd="15dp">
        <TextView
            android:id="@+id/lblWorkoutsTitle"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/workouts"/>

        <Button
            android:id="@+id/btnAddWorkout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/lblWorkoutsTitle"
            android:layout_marginTop="5dp"
            android:layout_marginBottom="5dp"
            android:padding="50dp"
            android:text="@string/add_workout"
            android:onClick="startAddWorkout"/>
    </LinearLayout>/>
</RelativeLayout>
