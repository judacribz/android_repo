package ca.uoit.csci4100u.userinterfaces;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by rfortier on 2017-09-27.
 */

public class LabelAdapter extends BaseAdapter {
    private String[] data;
    private Context context;

    LabelAdapter(Context context, String[] data) {
        this.context = context;
        this.data = data;
    }

    public int getCount() {
        return data.length;
    }

    public Object getItem(int position) {
        return data[position];
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View reusableView, ViewGroup parent) {
        String textToShow = this.data[position];

        TextView label = null;

        if (reusableView != null) {
            // reuse this view
            label = (TextView)reusableView;
        } else {
            // create a new view (this is the first item)
            label = new TextView(context);
        }

        label.setText(textToShow);

        return label;
    }
}
