package ca.uoit.csci4100u.userinterfaces;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void relative(View source) {
        Log.i("UIDemo", "relative() called.");

        setContentView(R.layout.sample_relative);
        populateProvinces();
    }

    private void populateProvinces() {
        // populate our spinner
        String[] data = getResources().getStringArray(R.array.provinces);
        LabelAdapter labelAdapter = new LabelAdapter(this, data);
        Spinner provincesSpinner = (Spinner)findViewById(R.id.spinner);
        provincesSpinner.setAdapter(labelAdapter);
    }
}
