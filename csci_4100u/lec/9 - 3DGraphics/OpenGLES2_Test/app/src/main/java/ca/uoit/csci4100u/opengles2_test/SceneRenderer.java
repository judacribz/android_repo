package ca.uoit.csci4100u.opengles2_test;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class SceneRenderer implements GLSurfaceView.Renderer {
    private Triangle triangle;
    private Square square;
    private TexturedSquare texturedSquare;
    private Mesh mesh;

    private float triangleRotationAngle = 0f;
    private float squareRotationAngle = 0f;
    private float texturedSquareRotationAngle = 0f;
    private float meshRotationAngle = 0f;

    private final float[] mvpMatrix = new float[16];
    private final float[] projectionMatrix = new float[16];
    private final float[] modelViewMatrix = new float[16];

    private Context context;

    public SceneRenderer(Context context) {
        triangleRotationAngle = 0f;
        squareRotationAngle = 0f;
        texturedSquareRotationAngle = 0f;
        meshRotationAngle = 0f;

        this.context = context;
    }

    public void onDrawFrame(GL10 unused) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        // point the camera
        Matrix.setLookAtM(modelViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
        Matrix.multiplyMM(mvpMatrix, 0, projectionMatrix, 0, modelViewMatrix, 0);

        // apply the rotation to the triangle
        float[] triangleRotationMatrix = new float[16];
        Matrix.setIdentityM(triangleRotationMatrix, 0);
        Matrix.translateM(triangleRotationMatrix, 0, -0f, -1f, 0f);
        Matrix.rotateM(triangleRotationMatrix, 0, triangleRotationAngle, 1f, 0f, 0f);

        // combine this with the model-view-projection matrix
        float[] triangleRotationVM = new float[16];
        Matrix.multiplyMM(triangleRotationVM, 0, mvpMatrix, 0, triangleRotationMatrix, 0);

        // apply the rotation and translation to the square
        float[] squareRotationMatrix = new float[16];
        Matrix.setIdentityM(squareRotationMatrix, 0);
        Matrix.translateM(squareRotationMatrix, 0, -0f, 1f, 0f);
        Matrix.rotateM(squareRotationMatrix, 0, squareRotationAngle, 0f, 1f, 0f);
        float[] squareRotationVM = new float[16];
        Matrix.multiplyMM(squareRotationVM, 0, mvpMatrix, 0, squareRotationMatrix, 0);

        meshRotationAngle += 0.2f;

        // apply the rotation to the mesh
        float[] meshRotationMatrix = new float[16];
        Matrix.setIdentityM(meshRotationMatrix, 0);
        Matrix.translateM(meshRotationMatrix, 0, 1f, 0f, 0f);
        Matrix.scaleM(meshRotationMatrix, 0, 0.25f, 0.25f, 0.25f);
        Matrix.rotateM(meshRotationMatrix, 0, meshRotationAngle, 1f, 0f, 1f);

        // combine this with the model-view-projection matrix
        float[] meshRotationVM = new float[16];
        Matrix.multiplyMM(meshRotationVM, 0, mvpMatrix, 0, meshRotationMatrix, 0);

        texturedSquareRotationAngle += 0.5f;

        // apply the rotation to the mesh
        float[] texturedSquareRotationMatrix = new float[16];
        Matrix.setIdentityM(texturedSquareRotationMatrix, 0);
        Matrix.translateM(texturedSquareRotationMatrix, 0, -1f, 0f, 0f);
        Matrix.rotateM(texturedSquareRotationMatrix, 0, texturedSquareRotationAngle, 1f, 1f, 0f);

        // combine this with the model-view-projection matrix
        float[] texturedSquareRotationVM = new float[16];
        Matrix.multiplyMM(texturedSquareRotationVM, 0, mvpMatrix, 0, texturedSquareRotationMatrix, 0);

        // draw the scene
        triangle.draw(triangleRotationVM);
        square.draw(squareRotationVM);
        texturedSquare.draw(texturedSquareRotationVM);
        //mesh.draw(meshRotationVM, meshRotationMatrix);
    }

    public float getTriangleRotationAngle() {
        return this.triangleRotationAngle;
    }

    public void setTriangleRotationAngle(float angle) {
        this.triangleRotationAngle = angle;
    }

    public float getSquareRotationAngle() {
        return this.squareRotationAngle;
    }

    public void setSquareRotationAngle(float angle) {
        this.squareRotationAngle = angle;
    }

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

        triangle = new Triangle();
        square = new Square(context);
        texturedSquare = new TexturedSquare(context);
        mesh = new Mesh(context, "old_computer.obj");
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        float ratio = (float) width / height;
        Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, -1, 1, 1, 10);
    }
}
