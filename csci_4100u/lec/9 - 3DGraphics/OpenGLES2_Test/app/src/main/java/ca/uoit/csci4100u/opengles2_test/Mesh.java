package ca.uoit.csci4100u.opengles2_test;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Mesh {
    private ObjLoader meshLoader;
    private FloatBuffer positionBuffer;
    private FloatBuffer normalBuffer;

    private int programId;

    private int vertexCount;

    static final int vertexShaderCodeFile = R.raw.mesh_vertex_shader;
    static final int fragmentShaderCodeFile = R.raw.mesh_fragment_shader;

    static final int COORDS_PER_VERTEX = 3;

    float colour[] = { 0.4f, 0.4f, 0.4f, 1.0f };
    float lightPosition[] = { -10f, -10f, -10f };

    public Mesh(Context context, String filename) {
        meshLoader = new ObjLoader(context, filename);

        ByteBuffer positionByteBuffer = ByteBuffer.allocateDirect(meshLoader.positions.length * 4);
        positionByteBuffer.order(ByteOrder.nativeOrder());
        positionBuffer = positionByteBuffer.asFloatBuffer();
        positionBuffer.put(meshLoader.positions);
        positionBuffer.position(0);

        vertexCount = meshLoader.positions.length / COORDS_PER_VERTEX;

        Log.i("3d", "Loaded " + vertexCount + " vertices.");

        ByteBuffer normalByteBuffer = ByteBuffer.allocateDirect(meshLoader.normals.length * 4);
        normalByteBuffer.order(ByteOrder.nativeOrder());
        normalBuffer = positionByteBuffer.asFloatBuffer();
        normalBuffer.put(meshLoader.normals);
        normalBuffer.position(0);

        // load and compile the shaders
        String vertexShaderCode = MyGLUtils.loadShaderFile(context, vertexShaderCodeFile);
        String fragmentShaderCode = MyGLUtils.loadShaderFile(context, fragmentShaderCodeFile);
        programId = MyGLUtils.loadProgram(vertexShaderCode, fragmentShaderCode);
    }

    public void draw(float[] mvpMatrix, float[] mvMatrix) {
        int COORDS_PER_NORMAL = 3;

        int vertexStride = COORDS_PER_VERTEX * 4;
        int normalStride = COORDS_PER_NORMAL * 4;

        Log.i("3d", "programId = " + programId);
        GLES20.glUseProgram(programId);

        // load the attribute data for the vertex shader

        int positionHandle = GLES20.glGetAttribLocation(programId, "aPosition");
        Log.i("3d", "positionHandle: "+ positionHandle);
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, positionBuffer);

        int normalHandle = GLES20.glGetAttribLocation(programId, "aNormal");
        Log.i("3d", "normalHandle: "+ normalHandle);
        GLES20.glEnableVertexAttribArray(normalHandle);
        GLES20.glVertexAttribPointer(normalHandle, COORDS_PER_NORMAL,
                GLES20.GL_FLOAT, false,
                normalStride, normalBuffer);

        // load the uniform data for the vertex shader

        int lightPosHandle = GLES20.glGetUniformLocation(programId, "uLightPos");
        Log.i("3d", "lightPosHandle: "+ lightPosHandle);
        GLES20.glUniform3fv(lightPosHandle, 1, lightPosition, 0);

        int colourHandle = GLES20.glGetUniformLocation(programId, "uColour");
        Log.i("3d", "colourHandle: "+ colourHandle);
        GLES20.glUniform4fv(colourHandle, 1, colour, 0);

        int mvpMatrixHandle = GLES20.glGetUniformLocation(programId, "uMVPMatrix");
        Log.i("3d", "mvpMatrixHandle: "+ mvpMatrixHandle);
        GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, mvpMatrix, 0);

        int mvMatrixHandle = GLES20.glGetUniformLocation(programId, "uMVMatrix");
        Log.i("3d", "mvMatrixHandle: "+ mvMatrixHandle);
        GLES20.glUniformMatrix4fv(mvMatrixHandle, 1, false, mvMatrix, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(normalHandle);
    }
}
