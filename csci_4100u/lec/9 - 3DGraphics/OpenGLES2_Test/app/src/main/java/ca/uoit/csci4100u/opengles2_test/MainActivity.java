package ca.uoit.csci4100u.opengles2_test;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends Activity implements View.OnTouchListener {

    private GLSurfaceView glView;
    private SceneRenderer renderer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // create a surface view (UI component to render to)
        glView = new GLSurfaceView(this);
        glView.setEGLContextClientVersion(2);

        // create a renderer for our scene
        renderer = new SceneRenderer(this);
        glView.setRenderer(renderer);

        glView.setOnTouchListener(this);

        // only for non-animated applications
        //glView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        setContentView(glView);
    }

    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float previousX;
    private float previousY;

    public boolean onTouch(View source, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            // figure out how much change in x and y
            float changeX = x - previousX;
            float changeY = y - previousY;

            if (y > glView.getHeight() / 2) {
                changeX = changeX * -1;
            }

            if (x < glView.getWidth() / 2) {
                changeY = changeY * -1;
            }

            float triangleAngle = renderer.getTriangleRotationAngle() + changeY * TOUCH_SCALE_FACTOR;
            renderer.setTriangleRotationAngle(triangleAngle);

            float squareAngle = renderer.getSquareRotationAngle() + changeX * TOUCH_SCALE_FACTOR;
            renderer.setSquareRotationAngle(squareAngle);

            glView.requestRender();
        }

        previousX = x;
        previousY = y;
        return true;
    }
}
