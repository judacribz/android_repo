package ca.uoit.csci4100u.opengles2_test;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Triangle {
    private FloatBuffer positionBuffer;
    private FloatBuffer colourBuffer;

    private int programId;

    private final String vertexShaderCode =
                    /*"attribute vec4 aPosition;" +
                    "void main() {" +
                    "  gl_Position = aPosition;" +
                    "}";*/
                    "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 aPosition;" +
                    "attribute vec4 aColour;" +
                    "varying vec4 vColour;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * aPosition;" +
                    "  vColour = aColour;" +
                    "}";

    private final String fragmentShaderCode =
                    "precision mediump float;" +
                    "varying vec4 vColour;" +
                    "void main() {" +
                    "  gl_FragColor = vColour;" +
                    "}";

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static float vertexPositions[] = {   // in counterclockwise order:
             0.0f,  0.5f, 0.0f, // top
            -0.5f, -0.5f, 0.0f, // bottom left
             0.5f, -0.5f, 0.0f  // bottom right
    };

    static final int VALUES_PER_COLOUR = 4;
    static float vertexColours[] = {
            1.0f, 0.0f, 0.0f, 1.0f, // red
            0.0f, 1.0f, 0.0f, 1.0f, // green
            0.0f, 0.0f, 1.0f, 1.0f // blue
    };

    public Triangle() {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertexPositions.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        positionBuffer = byteBuffer.asFloatBuffer();
        positionBuffer.put(vertexPositions);
        positionBuffer.position(0);

        ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(vertexColours.length * 4);
        byteBuffer2.order(ByteOrder.nativeOrder());
        colourBuffer = byteBuffer2.asFloatBuffer();
        colourBuffer.put(vertexColours);
        colourBuffer.position(0);

        programId = MyGLUtils.loadProgram(vertexShaderCode, fragmentShaderCode);
    }

    private int positionHandle;
    private int colourHandle;
    private int mvpMatrixHandle;

    private final int vertexCount = vertexPositions.length / COORDS_PER_VERTEX;
    private final int vertexStride = COORDS_PER_VERTEX * 4;
    private final int colourStride = VALUES_PER_COLOUR * 4;

    public void draw(float[] mvpMatrix) {
        GLES20.glUseProgram(programId);

        positionHandle = GLES20.glGetAttribLocation(programId, "aPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, positionBuffer);

        colourHandle = GLES20.glGetAttribLocation(programId, "aColour");
        GLES20.glEnableVertexAttribArray(colourHandle);
        GLES20.glVertexAttribPointer(colourHandle, VALUES_PER_COLOUR,
                GLES20.GL_FLOAT, false,
                colourStride, colourBuffer);
        /*
        colourHandle = GLES20.glGetUniformLocation(programId, "uColour");
        GLES20.glUniform4fv(colourHandle, 1, colour, 0);
        */

        mvpMatrixHandle = GLES20.glGetUniformLocation(programId, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, mvpMatrix, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        GLES20.glDisableVertexAttribArray(positionHandle);
    }
}
