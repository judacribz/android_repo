package ca.uoit.csci4100u.opengles2_test;

import android.content.Context;
import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class TexturedSquare {
    private FloatBuffer positionBuffer;
    private ShortBuffer indexBuffer;
    private FloatBuffer textureCoordBuffer;

    private int programId;

    static final int vertexShaderCodeFile = R.raw.textured_square_vertex_shader;
    static final int fragmentShaderCodeFile = R.raw.textured_square_fragment_shader;

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    final float vertexPositions[] = {
            -0.5f,  0.5f, 0.0f,   // 0: top left
            -0.5f, -0.5f, 0.0f,   // 1: bottom left
             0.5f, -0.5f, 0.0f,   // 2: bottom right
             0.5f,  0.5f, 0.0f }; // 3: top right

    private final int vertexCount = vertexPositions.length / COORDS_PER_VERTEX;

    private short vertexIndices[] = { 0, 1, 2, 0, 2, 3 };

    static final int TEXTURE_COORDS_PER_VERTEX = 2;
    final float[] vertexTextureCoords = {
                    0.0f, 1.0f,   // top left
                    0.0f, 0.0f,   // bottom left
                    1.0f, 0.0f,   // bottom right
                    1.0f, 1.0f }; // top right

    private int textureDataHandle;

    public TexturedSquare(Context context) {
        ByteBuffer positionByteBuffer = ByteBuffer.allocateDirect(vertexPositions.length * 4);
        positionByteBuffer.order(ByteOrder.nativeOrder());
        positionBuffer = positionByteBuffer.asFloatBuffer();
        positionBuffer.put(vertexPositions);
        positionBuffer.position(0);

        ByteBuffer indexByteBuffer = ByteBuffer.allocateDirect(vertexIndices.length * 2);
        indexByteBuffer.order(ByteOrder.nativeOrder());
        indexBuffer = indexByteBuffer.asShortBuffer();
        indexBuffer.put(vertexIndices);
        indexBuffer.position(0);

        ByteBuffer textureCoordByteBuffer = ByteBuffer.allocateDirect(vertexTextureCoords.length * 4);
        textureCoordByteBuffer.order(ByteOrder.nativeOrder());
        textureCoordBuffer = textureCoordByteBuffer.asFloatBuffer();
        textureCoordBuffer.put(vertexTextureCoords);
        textureCoordBuffer.position(0);

        // load and compile the shaders
        String vertexShaderCode = MyGLUtils.loadShaderFile(context, vertexShaderCodeFile);
        String fragmentShaderCode = MyGLUtils.loadShaderFile(context, fragmentShaderCodeFile);
        programId = MyGLUtils.loadProgram(vertexShaderCode, fragmentShaderCode);

        // prepare the texture
        textureDataHandle = MyGLUtils.loadTexture(context, R.drawable.logo);
    }

    public void draw(float[] mvpMatrix) {
        int vertexStride = COORDS_PER_VERTEX * 4;
        int textureStride = TEXTURE_COORDS_PER_VERTEX * 4;

        GLES20.glUseProgram(programId);

        int positionHandle = GLES20.glGetAttribLocation(programId, "aPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                0, positionBuffer);

        int textureCoordinateHandle = GLES20.glGetAttribLocation(programId, "aTextureCoordinate");
        GLES20.glEnableVertexAttribArray(textureCoordinateHandle);
        GLES20.glVertexAttribPointer(textureCoordinateHandle, TEXTURE_COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                0, textureCoordBuffer);

        // bind the texture data
        int textureHandle = GLES20.glGetUniformLocation(programId, "uTexture");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureDataHandle);
        GLES20.glUniform1i(textureHandle, 0);

        int mvpMatrixHandle = GLES20.glGetUniformLocation(programId, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, mvpMatrix, 0);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, vertexIndices.length,
                GLES20.GL_UNSIGNED_SHORT, indexBuffer);

        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(textureCoordinateHandle);
    }

}
