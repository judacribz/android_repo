uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform vec3 uLightPos;
uniform vec4 uColour;

attribute vec4 aPosition;
attribute vec3 aNormal;
 
varying vec4 vColour;
 
void main() {
   // Transform the vertex into eye space.
   vec3 modelViewVertex = vec3(uMVMatrix * aPosition);

   // Transform the normal's orientation into eye space.
   vec3 modelViewNormal = vec3(uMVMatrix * vec4(aNormal, 0.0));

   // Will be used for attenuation.
   float distance = length(uLightPos - modelViewVertex);

   // Get a lighting direction vector from the light to the vertex.
   vec3 lightVector = normalize(uLightPos - modelViewVertex);

   // Calculate the dot product of the light vector and vertex normal. If the normal and light vector are
   // pointing in the same direction then it will get max illumination.
   float diffuse = max(dot(modelViewNormal, lightVector), 0.1);

   // Attenuate the light based on distance.
   diffuse = diffuse * (1.0 / (1.0 + (0.25 * distance * distance))) + 0.2;

   // Multiply the color by the illumination level. It will be interpolated across the triangle.
   vColour = uColour * diffuse * 2;

   // gl_Position is a special variable used to store the final position.
   // Multiply the vertex by the matrix to get the final point in normalized screen coordinates.
   gl_Position = uMVPMatrix * aPosition;
}