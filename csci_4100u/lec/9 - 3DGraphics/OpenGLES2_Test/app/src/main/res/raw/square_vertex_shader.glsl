uniform mat4 uMVPMatrix;
uniform vec4 uColour;

attribute vec4 aPosition;

void main() {
   gl_Position = uMVPMatrix * aPosition;
}