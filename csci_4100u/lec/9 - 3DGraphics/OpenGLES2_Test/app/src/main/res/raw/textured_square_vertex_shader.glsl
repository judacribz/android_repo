uniform mat4 uMVPMatrix;
uniform sampler2D uTexture;

attribute vec4 aPosition;
attribute vec2 aTextureCoordinate;

varying vec2 vTextureCoordinate;

void main() {
   gl_Position = uMVPMatrix * aPosition;
   vTextureCoordinate = aTextureCoordinate;
}