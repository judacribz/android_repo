package ca.uoit.csci4100u.mapsdemo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private double latitude, longitude;
    private String locationName;

    private Bitmap spriteSheet;

    final int SPRITE_WIDTH  = 64;
    final int SPRITE_HEIGHT = 64;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // here I do *not* have a map

        // collect the input values (location to show)
        Intent callingIntent = getIntent();
        latitude = callingIntent.getDoubleExtra("backupLatitude", 0.0);
        longitude = callingIntent.getDoubleExtra("backupLongitude", 0.0);
        locationName = callingIntent.getStringExtra("locationName");
        Address locationAddress = forwardGeocode(locationName);
        if (locationAddress != null) {
            latitude = locationAddress.getLatitude();
            longitude = locationAddress.getLongitude();
        }

        // load the image to be drawn to the map
        spriteSheet = BitmapFactory.decodeResource(getResources(), R.drawable.explosion);

    }

    private Address forwardGeocode(String locationName) {
        if (Geocoder.isPresent()) {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());

            try {
                List<Address> results = geocoder.getFromLocationName(locationName, 1);

                if (results.size() > 0) {
                    return results.get(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng position = new LatLng(latitude, longitude);

        // zoom to an appropriate level
        // centre the map around the specified location
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 15f) );

        // add a marker at the specified location
        MarkerOptions options = new MarkerOptions();
        mMap.addMarker(options.position(position).title(locationName));

        // configure the map settings
        mMap.setTrafficEnabled(true);
        mMap.setBuildingsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        // enable the zoom controls
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);


        drawOnMap(position);
    }

    private void drawOnMap(LatLng position) {
        int width = 1000, height = 1000;
        // draw to a bitmap
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawOnCanvas(canvas);

        // draw the bitmap as a ground overlay
        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        GroundOverlayOptions options = new GroundOverlayOptions();
        options.image(bitmapDescriptor);
        options.position(position, width);
        options.transparency(0.5f);
        GroundOverlay groundOverlay = mMap.addGroundOverlay(options);
    }

    private void drawOnCanvas(Canvas canvas) {
        // draw in black, unfilled shapes
        Paint blackLine = new Paint();
        blackLine.setARGB(255, 0, 0, 0);
        blackLine.setStyle(Paint.Style.STROKE);

        // draw a black line
        canvas.drawLine(100, 550, 600, 800, blackLine);

        // draw in blue, unfilled shapes
        Paint blueLine = new Paint();
        blueLine.setARGB(255, 0, 0, 255);
        blueLine.setStyle(Paint.Style.STROKE);

        // draw a blue rectangle
        RectF bounds = new RectF(100, 800, 300, 950);
        canvas.drawRect(bounds, blueLine);

        // draw in green, filled shapes
        Paint greenFill = new Paint();
        greenFill.setARGB(255, 0, 255, 0);
        greenFill.setStyle(Paint.Style.FILL);

        // draw a green filled rectangle
        bounds = new RectF(400, 100, 500, 250);
        canvas.drawRect(bounds, greenFill);

        // draw in yellow, filled shapes
        Paint yellowFill = new Paint();
        yellowFill.setARGB(255, 255, 255, 0);
        yellowFill.setStyle(Paint.Style.FILL);

        // draw a yellow filled rounded rectangle
        bounds = new RectF(100, 400, 300, 450);
        canvas.drawRoundRect(bounds, 10f, 10f, yellowFill);

        // draw in cyan, filled shapes
        Paint cyanFill = new Paint();
        cyanFill.setARGB(255, 0, 255, 255);
        cyanFill.setStyle(Paint.Style.FILL);

        // draw a cyan filled circle
        canvas.drawCircle(500, 800, 150, cyanFill);

        // draw in magenta, filled shapes
        Paint magentaFill = new Paint();
        magentaFill.setARGB(255, 255, 0, 255);
        magentaFill.setStyle(Paint.Style.FILL);

        // draw a magenta filled oval/ellipse
        bounds = new RectF(450, 400, 750, 450);
        canvas.drawOval(bounds, magentaFill);

        // draw in red, filled shapes
        Paint redFill = new Paint();
        redFill.setARGB(255, 255, 0, 0);
        redFill.setStyle(Paint.Style.FILL);

        // draw a red pie
        bounds = new RectF(650, 200, 820, 380);
        canvas.drawArc(bounds, 60, 290, true, redFill);

        // draw some white text
        float density = getResources().getDisplayMetrics().density;
        Paint font = new Paint();
        font.setColor(Color.WHITE);
        font.setTextSize(density * 32);
        font.setStyle(Paint.Style.FILL);
        canvas.drawText("2D Graphics Sample", 500, 600, font);
    }
}
