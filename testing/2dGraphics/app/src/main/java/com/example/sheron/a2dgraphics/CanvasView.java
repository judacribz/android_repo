package com.example.sheron.a2dgraphics;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.jar.Attributes;

public class CanvasView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas_view);
    }

    public CanvasView(Context context, Attributes attributes) {
        super(context, attributes);
    }
}
