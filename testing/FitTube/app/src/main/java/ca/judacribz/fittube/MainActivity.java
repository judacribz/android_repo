package ca.judacribz.fittube;


import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;

import java.io.IOException;
import java.util.HashMap;

import static ca.judacribz.fittube.Quickstart.getYouTubeService;

public class MainActivity extends AppCompatActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        YouTube service;
        try {
            service = getYouTubeService();
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("part", "snippet");
            parameters.put("maxResults", "25");
            parameters.put("q", "surfing");
            parameters.put("type", "");

            YouTube.Search.List searchListByKeywordRequest = service.search().list(parameters.get("part"));
            if (parameters.containsKey("maxResults")) {
                searchListByKeywordRequest.setMaxResults(Long.parseLong(parameters.get("maxResults")));
            }

            if (parameters.containsKey("q") && !parameters.get("q").isEmpty()) {
                searchListByKeywordRequest.setQ(parameters.get("q"));
            }

            if (parameters.containsKey("type") && !parameters.get("type").isEmpty()) {
                searchListByKeywordRequest.setType(parameters.get("type"));
            }

            SearchListResponse response = searchListByKeywordRequest.execute();
            System.out.println(response);

            Toast.makeText(this, response.toString(), Toast.LENGTH_SHORT).show();
        } catch (IOException io) {
            Toast.makeText(this, "yop", Toast.LENGTH_SHORT).show();
        }
    }
}