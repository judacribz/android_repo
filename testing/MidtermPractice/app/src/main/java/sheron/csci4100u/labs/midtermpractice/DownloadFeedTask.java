package sheron.csci4100u.labs.midtermpractice;

import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DownloadFeedTask extends AsyncTask<String, Void, ArrayList<Story>> {

    private StoryDataListener storyDataListener;

    void setStoryDataListener(StoryDataListener storyDataListener) {

        this.storyDataListener = storyDataListener;
    }

    @Override
    protected ArrayList<Story> doInBackground(String... urls) {
        ArrayList<Story> stories = new ArrayList<>();
        try {
            // parse out the data
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
            URL url = new URL(urls[0]);
            Document document = docBuilder.parse(url.openStream());
            document.getDocumentElement().normalize();

            // look for <WordDefinition> tags
            NodeList main = document.getElementsByTagName("entry");
            if ((main.getLength() > 0) && (main.item(0).getNodeType() == Node.ELEMENT_NODE)) {


                for (int i=0; i <main.getLength(); i++) {

                    Element definitions = (Element)main.item(i);

                    stories.add(new Story(definitions.getElementsByTagName("title").item(0).getTextContent(),
                            definitions.getElementsByTagName("author").item(0).getTextContent(),
                            definitions.getElementsByTagName("summary").item(0).getTextContent()));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return stories;
    }

    @Override
    protected void onPostExecute(ArrayList<Story> stories) {

        // send back new data
        storyDataListener.showStories(stories);
    }
}