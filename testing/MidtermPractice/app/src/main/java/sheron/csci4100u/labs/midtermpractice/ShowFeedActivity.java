package sheron.csci4100u.labs.midtermpractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ShowFeedActivity extends AppCompatActivity implements StoryDataListener{

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_feed);

        listView = findViewById(R.id.lstFeed);



        DownloadFeedTask task = new DownloadFeedTask();
        task.setStoryDataListener(this);
        String url = getIntent().getStringExtra("URL");
        task.execute(url);


    }

    @Override
    public void showStories(ArrayList<Story> data) {
        StoryAdapter storyAdapter = new StoryAdapter(this, data);
        listView.setAdapter(storyAdapter);
    }
}
