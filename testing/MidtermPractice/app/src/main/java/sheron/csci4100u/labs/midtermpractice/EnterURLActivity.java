package sheron.csci4100u.labs.midtermpractice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EnterURLActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_url);

        ((Button)findViewById(R.id.btnShowFeed)).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        String url = "http://www.theregister.co.uk/software/os/headlines.atom";
        Intent showFeedIntent = new Intent(this, ShowFeedActivity.class);
        showFeedIntent.putExtra("URL", url);
        startActivity(showFeedIntent);
    }

}
