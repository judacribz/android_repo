package sheron.csci4100u.labs.midtermpractice;

import java.util.ArrayList;

public interface StoryDataListener {
    public void showStories(ArrayList<Story> data);
}