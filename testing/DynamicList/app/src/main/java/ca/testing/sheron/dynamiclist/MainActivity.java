package ca.testing.sheron.dynamiclist;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{

    SetsAdapter setAdapter;
    RecyclerView setList;
    ArrayList<Set> sets;
    ViewGroup main;
    ViewGroup title;
    LayoutInflater inflater;
    View view;
    TextView exerciseName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sets = new ArrayList<>();

        int i = 1;
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));
        sets.add(new Set(i++, 5, 135));


        title = (ViewGroup) findViewById(R.id.title_insert_point);
        main = (ViewGroup) findViewById(R.id.insert_point);

        inflater = getLayoutInflater();

        for (int id = 101; id < 110; id ++) {
            insertWidgets(id);
        }

//
//        //update a list
//        sets.add(new Set(i++, 5, 335));
//        sets.add(new Set(i++, 5, 335));
//        sets.add(new Set(i++, 5, 135));
//        setList = (RecyclerView) findViewById(107);
//        setList.setHasFixedSize(true);
//        setList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        setAdapter = new SetsAdapter(sets);
//        setList.setAdapter(setAdapter);

    }


    @SuppressLint("InflateParams")
    public void insertWidgets(int id) {
        view = inflater.inflate(R.layout.sets_subtitle_item, null);
        title.addView(view, 0);


        view = inflater.inflate(R.layout.listview_item, null);
        view.setId(id);
        main.addView(view, 0);

        setList = (RecyclerView) view.findViewById(R.id.rvExerciseSets);
        exerciseName = (TextView) view.findViewById(R.id.lblExerciseName);
        exerciseName.setText("SDFSDF");
        setList.setHasFixedSize(true);
        setList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        setAdapter = new SetsAdapter(sets);
        setList.setAdapter(setAdapter);
    }
}
