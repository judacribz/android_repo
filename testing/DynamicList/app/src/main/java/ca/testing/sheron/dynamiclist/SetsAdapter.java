package ca.testing.sheron.dynamiclist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

class SetsAdapter extends RecyclerView.Adapter<SetViewHolder> {
    private int numberOfSets;
    private ArrayList<Set> sets;

    SetsAdapter(ArrayList<Set> sets) {
        this.sets = sets;
        this.numberOfSets = sets.size();
    }

    @Override
    public SetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new SetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SetViewHolder holder, int position) {

        Set set = sets.get(position);
        holder.bind(set.getSets()+"", set.getReps()+"", set.getWeight()+"");
    }

    @Override
    public int getItemCount() {
        return numberOfSets;
    }
}
