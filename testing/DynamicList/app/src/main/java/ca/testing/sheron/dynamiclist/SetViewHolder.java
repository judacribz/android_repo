package ca.testing.sheron.dynamiclist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


class SetViewHolder extends RecyclerView.ViewHolder {
    private TextView tvSets, tvReps, tvWeight;

    SetViewHolder(View itemView) {
        super(itemView);
        tvSets = itemView.findViewById(R.id.tvSets);
        tvReps = itemView.findViewById(R.id.etReps);
        tvWeight = itemView.findViewById(R.id.etWeight);
    }

    void bind(String sets, String reps, String weight) {
        tvSets.setText(sets);
        tvReps.setText(reps);
        tvWeight.setText(weight);
    }

}
