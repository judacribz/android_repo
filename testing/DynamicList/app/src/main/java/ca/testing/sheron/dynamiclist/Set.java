package ca.testing.sheron.dynamiclist;


class Set {
    private int sets, reps;
    private float weight;

    public Set(int sets, int reps, float weight) {
        this.sets = sets;
        this.reps = reps;
        this.weight = weight;
    }

    public int getSets() {
        return sets;
    }

    public int getReps() {
        return reps;
    }

    public float getWeight() {
        return weight;
    }
}
