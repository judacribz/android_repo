package sheron.projects.snippets.recyclerview;

public class Exercise {
    String name;
    String type;
    int reps;
    int sets;
    float weight;

    public Exercise(String type, int sets, int reps, float weight) {
        this.type = type;
        this.reps = reps;
        this.sets = sets;
        this.weight = weight;
    }

    public Exercise(String name, String type, int sets, int reps, float weight) {
        this.name = name;
        this.type = type;
        this.reps = reps;
        this.sets = sets;
        this.weight = weight;
    }
    public Exercise() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public int getReps() {
        return reps;
    }

    public int getSets() {
        return sets;
    }

    public float getWeight() {
        return weight;
    }
}
