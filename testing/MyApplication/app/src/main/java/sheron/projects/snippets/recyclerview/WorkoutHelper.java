package sheron.projects.snippets.recyclerview;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;

public class WorkoutHelper extends SQLiteOpenHelper {
    static final int DATABASE_VERSION = 1;
    Context context;

    static final String TABLE = "workouts";

    static final String CREATE_STATEMENT =
            "CREATE TABLE workouts (\n" +
                    "      _id integer primary key autoincrement,\n" +
                    "      workoutName text not null,\n" +
                    "      exerciseName text not null,\n" +
                    "      exerciseType text not null,\n" +
                    "      sets integer not null,\n" +
                    "      reps integer not null,\n" +
                    "      weight real not null,\n" +
                    "      email varchar2(100) null\n" +
                    ")\n";

    static final String DROP_STATEMENT = "DROP TABLE workouts";

    public WorkoutHelper(Context context) {
        super(context, "workouts", null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the database, using CREATE SQL statement
        db.execSQL(CREATE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,
                          int oldVersionNum,
                          int newVersionNum) {
        // delete the old database
        db.execSQL(DROP_STATEMENT);

        // re-create the database
        db.execSQL(CREATE_STATEMENT);
    }

    // CRUD functions

    // CREATE
    public void createWorkout(Workout workout, String email) {
        ArrayList<Exercise> exercises = workout.getExercises();

        // put that data into the database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();

        for (Exercise exercise : exercises) {
            newValues.put("workoutName", workout.getName());
            newValues.put("exerciseName", exercise.getName());
            newValues.put("exerciseType", exercise.getType());
            newValues.put("sets", exercise.getSets());
            newValues.put("reps", exercise.getReps());
            newValues.put("weight", exercise.getWeight());
            newValues.put("email", email);

            db.insert(TABLE, null, newValues);

            Toast.makeText(context, workout.getName(), Toast.LENGTH_SHORT).show();
        }
    }

    // READ
    public Workout getWorkout(String workoutName, String email) {
        Workout workout = null;
        ArrayList<Exercise> exercises = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = new String[] {"exerciseName", "exerciseType", "sets", "reps", "weight"};
        String where = "workoutName = ? AND email = ?";
        String[] whereArgs = new String[] { "" + workoutName, "" + email};
        Cursor cursor = db.query(TABLE, columns, where, whereArgs, "", "", "");


        if (cursor.getCount() >= 1) {
            cursor.moveToFirst();

            do {
                exercises.add(new Exercise(cursor.getString(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3), cursor.getFloat(4)));
            } while (cursor.moveToNext());

            workout = new Workout(exercises, workoutName);
        }

        cursor.close();

        return workout;
    }

    public ArrayList<Workout> getAllWorkouts(String email) {
        ArrayList<Workout> workouts = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String[] column = new String[] {"workoutName"};
        Cursor workoutCursor = db.query(true, TABLE, column, null, null, "workoutName", null, null, null);

        workoutCursor.moveToFirst();
        if (workoutCursor.getCount() >= 1) {
            do {
                workouts.add(getWorkout(workoutCursor.getString(0), email));
            } while (!workoutCursor.moveToNext());
        }

        workoutCursor.close();
        return workouts;
    }

//    // UPDATE
//    public boolean updateWorkout(Workout workout) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("firstName", workout.getFirstName());
//        values.put("lastName", workout.getLastName());
//        values.put("email", workout.getEmail());
//        values.put("phone", workout.getPhone());
//
//        int numRows = db.update(TABLE, values, "_id = ?", new String[] { "" + workout.getId() });
//
//        return (numRows == 1);
//    }

    // DELETE
    public boolean deleteWorkout(long id) {
        SQLiteDatabase db = this.getWritableDatabase();

        int numRows = db.delete(TABLE, "_id = ?", new String[] { "" + id });

        return (numRows == 1);
    }

    public void deleteAllWorkouts() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE, "", new String[] { });
    }
}
