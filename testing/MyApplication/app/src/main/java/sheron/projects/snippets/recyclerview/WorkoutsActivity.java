package sheron.projects.snippets.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Map;


public class WorkoutsActivity extends AppCompatActivity {

    private final static String TAG_WORKOUTS_ACTIVITY = "TAG_WORKOUTS_ACTIVITY";

    ButtonAdapter workoutAdapter;
    RecyclerView workoutsList;
    LinearLayoutManager layoutManager;
    ArrayList<String> localWorkouts;
    ArrayList<String> firebaseWorkouts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts);

        localWorkouts = null;

        workoutsList = (RecyclerView) findViewById(R.id.rvButtons);

        final WorkoutHelper workoutHelper = new WorkoutHelper(WorkoutsActivity.this);

        // Set the layout manager for the localWorkouts
        layoutManager = new LinearLayoutManager(this);
        workoutsList.setLayoutManager(layoutManager);
        workoutsList.setHasFixedSize(true);


        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference().child("default_workouts");

        // Listener for default_workouts object in firebase
        myRef.addValueEventListener(new ValueEventListener() {

            // Saves data to file
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {

                    ArrayList<Workout> workouts = new ArrayList<>();
                    ArrayList<Exercise> exercises = new ArrayList<>();
                    firebaseWorkouts = new ArrayList<>();
                    for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                        String workoutName = postSnapshot.getKey();
                        firebaseWorkouts.add(workoutName);

                        Exercise exercise;
                        for (DataSnapshot snap : postSnapshot.getChildren()) {
                            exercise = snap.getValue(Exercise.class);
                            exercise.setName(snap.getKey());
                            exercises.add(exercise);
                        }

                        workouts.add(new Workout(exercises, workoutName));
                    }



                    if (firebaseWorkouts.size() > 0) {
                        displayBtnList(firebaseWorkouts);

                        for (Workout workout: workouts) {

                            if (workoutHelper.getWorkout(workout.getName(), "sheron.balas@gmail.com") == null) {
                                workoutHelper.createWorkout(workout, "sheron.balas@gmail.com");
                            }
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG_WORKOUTS_ACTIVITY, databaseError.getDetails());
            }
        });


//        if (localWorkouts != null && firebaseWorkouts != null) {
//            if (localWorkouts.size() >= firebaseWorkouts.size()) {
//                displayBtnList(localWorkouts);
//            } else {
//                displayBtnList(firebaseWorkouts);
//            }
//        }
    }

    void displayBtnList(ArrayList<String> workouts) {
        workoutAdapter = new ButtonAdapter(workouts);
        workoutsList.setAdapter(workoutAdapter);
    }


}
