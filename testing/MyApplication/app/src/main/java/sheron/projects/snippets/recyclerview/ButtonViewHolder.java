package sheron.projects.snippets.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;


public class ButtonViewHolder extends RecyclerView.ViewHolder {
    Button listItemButtonView;

    public ButtonViewHolder(View itemView) {
        super(itemView);
        listItemButtonView = itemView.findViewById(R.id.btnListItem);
    }

    void bind(String btnText) {
        listItemButtonView.setText(btnText);
    }
}
