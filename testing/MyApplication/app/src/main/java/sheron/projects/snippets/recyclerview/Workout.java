package sheron.projects.snippets.recyclerview;

import java.util.ArrayList;

class Workout {
    ArrayList<Exercise> exercises;
    String name;


    public Workout(ArrayList<Exercise> exercises, String name) {
        this.exercises = exercises;
        this.name = name;

    }

    public ArrayList<Exercise> getExercises() {
        return exercises;
    }

    public String getName() {
        return name;
    }
}
