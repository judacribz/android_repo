package sheron.projects.snippets.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

class ButtonAdapter extends RecyclerView.Adapter<ButtonViewHolder> {
    private int numberOfBUttons;
    private ArrayList<String>  btnNames;

    public ButtonAdapter(ArrayList<String> btnNames) {
        this.numberOfBUttons = btnNames.size();
        this.btnNames = btnNames;
    }

    @Override
    public ButtonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.button_list_item, parent, false);
        return new ButtonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ButtonViewHolder holder, int position) {

        holder.bind(btnNames.get(position));
    }

    @Override
    public int getItemCount() {
        return numberOfBUttons;
    }
}
