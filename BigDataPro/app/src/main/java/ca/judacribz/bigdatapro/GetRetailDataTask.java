package ca.judacribz.bigdatapro;

import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class GetRetailDataTask extends AsyncTask<String, Void, SparseArray<ArrayList<Integer>>> {

    private RetailDataObserver licenseObserver;


    public GetRetailDataTask(RetailDataObserver licenseObserver) {
        this.licenseObserver = licenseObserver;
    }

    @Override
    protected SparseArray<ArrayList<Integer>> doInBackground(String... urls) {
        SparseArray<ArrayList<Integer>> licenseData = new SparseArray<>();

        try {
            // connect to the website
            URL url = new URL(urls[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // download the data
            InputStream inputRaw = conn.getInputStream();
            BufferedReader input = new BufferedReader(new InputStreamReader(inputRaw));

            // Store data in String licenseData
            String line;
            int i = 0;
            ArrayList<Integer> items;
            String[] itemStrs;

            while ((line = input.readLine()) != null) {
                itemStrs = line.split(" ");

                items = new ArrayList<>();
                for (String g : itemStrs) {
                    items.add(Integer.valueOf(g));
                }

                licenseData.append(i++, items);
            }

            // Close connection
            inputRaw.close();
            conn.disconnect();
        } catch (IOException io) {
            io.printStackTrace();
        }

        return licenseData;
    }

    @Override
    protected void onPostExecute(SparseArray<ArrayList<Integer>> arrayListSparseArray) {
        licenseObserver.licenseDataUpdated(arrayListSparseArray);
    }
}
