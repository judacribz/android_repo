package ca.judacribz.bigdatapro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RetailDataObserver{
    String url;
    SparseArray<ArrayList<Integer>> licenseData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        url = getResources().getString(R.string.data_file);

        // Initiate the download (AsyncTask)
        GetRetailDataTask task = new GetRetailDataTask(this);
        task.execute(url);
    }

    @Override
    public void licenseDataUpdated(SparseArray<ArrayList<Integer>> licenseData) {
        Toast.makeText(this, "" + (licenseData.get(0)).get(0), Toast.LENGTH_SHORT).show();
        this.licenseData = licenseData;
    }
}
