package ca.judacribz.bigdatapro;


import android.util.SparseArray;

import java.util.ArrayList;

public interface RetailDataObserver {
    void licenseDataUpdated(SparseArray<ArrayList<Integer>> licenseData);
}